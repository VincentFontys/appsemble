Add support for specifying which storage mechanism to use for the `storage` actions. Supported
values are `indexedDB`, `localStorage`, and `sessionStorage`.

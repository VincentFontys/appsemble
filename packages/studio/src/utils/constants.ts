/**
 * The supported languages in Appsemble Studio.
 */
export const supportedLanguages = {
  da: 'Danish (Dansk)',
  de: 'German (Deutsch)',
  fr: 'French (Français)',
  hr: 'Croatian (Hrvatski)',
  nl: 'Dutch (Nederlands)',
  en: 'English',
};

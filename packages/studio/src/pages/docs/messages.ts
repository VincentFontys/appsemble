import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: 'Documentation',
  reference: 'Reference',
  app: 'App',
  action: 'Action',
  remapper: 'Remapper',
});
